<?php

// For validation
use Respect\Validation\Validator as v;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__.'/vendor/autoload.php';
require_once 'config.php';

use \Slim\App;

session_start();

/**
 * Administrator UI
 */
$app->group('/admin', function (App $app) {
    return new \App\Controllers\AdminController($app);
})->add( new \App\Controllers\SessionAuthenticationController($app) );


/**
 * Backend API
 */
$app->group('/api', function (App $app) {
    return new \App\Controllers\APIController($app);
})
    ->add( new \App\Controllers\APIAuthenticationController($app) );

    
$app->add(function ($req, $res, $next) {
    return $next($req, $res
        ->withHeader('Vary', 'Origin')
        ->withHeader('Access-Control-Allow-Origin', $req->getHeader('Origin'))
        ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
        ->withHeader('Access-Control-Allow-Credentials', 'true')
    );
});

$app->run();

<?php

// For validation
use Respect\Validation\Validator as v;


// Force UTC timezone for compatible database
date_default_timezone_set('UTC');

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$container = new \Slim\Container($configuration);
$app = new \Slim\App($container);

$container = $app->getContainer();

// Database dependency
$container["db"] = function ($c) {
    $config = new \Doctrine\DBAL\Configuration();

    $connectionParams = array(
        'dbname' => 'sms_mms_app',
        'user' => 'root',
        'password' => '',
        'host' => 'database',
        'driver' => 'pdo_mysql',
    );

    return \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
};

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $response->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write($exception);
    };
};

// View dependency
$container['view'] = new \Slim\Views\PhpRenderer('templates/');


interface PasswordHandler {
    public function getPasswordData($plain_password);
    public function validatePassword($plain_password, $password_data);
}

class HashPasswordHandler implements PasswordHandler {
    public function getPasswordData($p) {
        // Need to enable mcrypt extension: $salt = mcrypt_create_iv(16, MCRYPT_DEV_URANDOM);
        $salt = mcrypt_create_iv(16, MCRYPT_DEV_URANDOM);

        return $this->encodePassword($p, $salt);
    }

    public function validatePassword($p, $d) {
        return $this->encodePassword($p, substr(base64_decode(substr($d,9)),64)) === $d;
    }

    private function encodePassword($p, $s) {
        return "{SSHA256}" . base64_encode(hash('sha256', $p . $s) . $s);
    }
}

$container["passwordEncoder"] = function($c) {
    return new HashPasswordHandler();
};

$container["generateToken"] = function () {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
};

// Access to Skyetel Integration credentials
$container["skyetelIntegration"] = function($c) {
    return [ 
        "sms_url" => ( getenv("SKYETEL_SMS_API_URL") === false ? "https://sms.skyetel.com/v1/out" : getenv("SKYETEL_SMS_API_URL") ),
        "api_sid" => getenv("SKYETEL_API_SID"),
        "api_secret" => getenv("SKYETEL_API_SECRET")
    ];
};

$container["attachmentURL"] = function($c) {
    return ( getenv("VIRTUAL_HOST") === false ? "https://example.com/backend/api/attachments/" : "https://" . getenv("VIRTUAL_HOST") . "/backend/api/attachments/" );
};

<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Container\ContainerInterface;


class APIAuthenticationController {
    private $app;
    private $c;

    const LOGIN_EXPIRATION = "NOW(6) + INTERVAL 12 HOUR";

    public function __construct($app) {
        $this->app = $app;
        $this->c = $app->getContainer();
    }

    function __invoke(Request $request, Response $response, $next) {
        //don't interfere with unmatched routes
        $route = $request->getAttribute('route');
        $path = $request->getUri()->getPath();

        $final_route = substr($path, strrpos($path, '/') + 1);

        if ($final_route === "login") {
            // Continue to Login
            return $next($request, $response);

        // Or attachments route
        } else if (array_slice(explode("/", $path), -2, 1)[0] === "attachments" ) {
            $attachments_route = true;
        } else {
            $attachments_route = false;
        }
        
        // Check for token or key
        $queryParams = $request->getQueryParams();

        // Handle inbound SMS/MMS
        if ( $final_route === "in" ) {

            $authorized_key = getenv("API_INBOUND_AUTH_KEY");

            if ( $authorized_key !== false
                && array_key_exists("key", $queryParams)
                && $queryParams["key"] === $authorized_key
            ) {
                // Continue
                return $next($request, $response);

            } else {
                // Failed authentication
                return $response
                    ->withStatus(401);
            }

        // Handle token
        } else if ( array_key_exists("token", $queryParams) ) {
            $user = $this->c->db->createQueryBuilder()
                ->select("id")
                ->from("users")
                ->where("active IS NOT NULL")
                ->andWhere("login_token_expiration > NOW(6)")
                ->andWhere("login_token = :token")
                ->setParameter(":token", $queryParams["token"])
                ->execute()
                ->fetch();

            if (is_array($user) && array_key_exists("id", $user) ) {

                // Check if the route happens to be logout
                $route = $request->getAttribute('route');
                $path = $request->getUri()->getPath();

                $final_route = substr($path, strrpos($path, '/') + 1);

                $request = $request->withAttribute("users.id", $user["id"]);

                // Store the token with an expiration
                $result = $this->c->db->createQueryBuilder()
                    ->update("users")
                    ->where("id = :id")
                    ->setParameter(":id", $user["id"])
                    ->set("login_token_expiration", self::LOGIN_EXPIRATION)
                    ->execute();

                // Continue
                return $next($request, $response);
            }
        
        // Exception for Attachments made Public
        } else if ( $attachments_route === true ) {
            
            // Continue
            return $next($request, $response);
        } 

        // Failed authentication
        return $response
            ->withStatus(401);
    }
}

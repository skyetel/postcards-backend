<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Container\ContainerInterface;


class SessionAuthenticationController {
    private $app;
    private $c;

    public function __construct($app) {
        $this->app = $app;
        $this->c = $app->getContainer();
    }

    function __invoke(Request $request, Response $response, $next) {
        if (!isset($_SESSION['__user'])) {
            //don't interfere with unmatched routes
            $route = $request->getAttribute('route');
            $path = $request->getUri()->getPath();

            $final_route = substr($path, strrpos($path, '/') + 1);

            if ($route && !in_array($final_route, ['login','logout'])) {
                return $response->withRedirect($this->c->router->pathFor('admin-logout'));
            }
        }

        return $next($request, $response);
    }
}

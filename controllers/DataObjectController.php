<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \App\Data\StatementSort;
use \App\Data\DataObjectAccess;
use \App\Auth\DataObjectAuthorizor;

use \App\Helpers\QueryParamHelper;
use \App\Helpers\StatementFilter;


abstract class DataObjectController implements DataObjectAccess {
    // Maximum returned records
    const MAX_LIMIT = 100;

    // Default number of returned records
    const DEFAULT_LIMIT = 50;

    private $app;
    protected $validator;
    protected $authorizor;
    protected $table;

    function __construct($app, DataObjectAuthorizor $authorizor = null) {
        $this->app = $app;
        $this->authorizor = $authorizor;
    }

    protected function getApp() {
        return $this->app;
    }

    protected function getAuthorizor() {
        return $this->authorizor;
    }

    protected function getContainer() {
        return $this->app->getContainer();
    }

    protected function getDB() {
        return $this->app->getContainer()->db;
    }

    /**
     * Manage a request to do something to an object
     */
    abstract public function manageObject (Request $request, Response $response, array $args);

    // Handle validation of new object
    private function validateNew( $fields = [] ) {
        // Validate all fields required and optional that are provided
        foreach ( $this->getValidator() as $v) {
            $v->assert($fields);
        }

        return true;
    }

    protected function getDataFields($fields = []) {
        return QueryParamHelper::getDataFields($fields);
    }

    protected function getFilterParam($query = []) {
        return QueryParamHelper::getFilterParam($query);
    }

    protected function getOffsetParam($query = []) {
        return QueryParamHelper::getOffsetParam($query);
    }

    protected function getLimitParam($query = []) {
        return QueryParamHelper::getLimitParam($query);
    }

    protected function getSortParam($query = []) {
        return array_key_exists("sort", $query) && is_string($query["sort"]) ? explode(",", $query["sort"]) : [];
    }

    public function getObjects($offset = 0, $limit = self::DEFAULT_LIMIT, $filters = [], $sort = []) {
        $limit = min($limit, self::MAX_LIMIT);

        $qb = $this->getDB()->createQueryBuilder();

        if ( !is_null($this->authorizor) ) {
            $stmt = $this->authorizor
                ->authorizeSelect(
                    $qb->from($this->table,"a")
                );

        } else {
            $stmt = $qb
                ->select('*')
                ->from($this->table, "a");
        }

        $stmt = $stmt
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        // Add filtering
        if ( count($filters) > 0 ) {
            $stmt = StatementFilter::setFilter($stmt, $filters);
        }

        // Add sorting
        if ( count($sort) > 0 ) {
            $stmt = StatementSort::setSort($stmt, $sort);
        }

        return $stmt->execute()->fetchAll();
    }

    public function getObject($id) {
        $qb = $this->getDB()->createQueryBuilder();

        if ( !is_null($this->authorizor) ) {
            return $this->authorizor
                ->authorizeSelect(
                    $qb->from($this->table, "a")
                )
                ->andWhere("a.id = :id")
                ->setParameter(":id", $id)
                ->execute()
                ->fetch();
        } else {
            return $qb
                ->select('*')
                ->from($this->table, "a")
                ->andWhere("a.id = :id")
                ->setParameter(":id", $id)
                ->execute()
                ->fetch() ;
        }
    }

    public function setObject($id, $fields = []) {
        $qb = $this->getDB()->createQueryBuilder();

        if ( !is_null($this->authorizor) ) {
            return $this->authorizor
                ->authorizeUpdate(
                    $qb->update($this->table),
                    $fields
                )
                ->where("id = :id")
                ->setParameter(":id", $id)
                ->execute() === 0;

        } else {
            $values = [];
            $params = [];

            foreach ($fields as $f => $v) {
                $values[$f] = ":$f";
                $params[":$f"] = $v;
            }

            $qb->update($this->table)
                ->where("id = :id")
                ->setParameter(":id", $id);

            foreach ($fields as $f => $v) {
                $qb->set($f, ":$f")
                    ->setParameter(":$f", $v);
            }

            return $qb
                ->execute() === 0;
        }
    }

    public function createObject($fields = []) {
        $qb = $this->getDB()->createQueryBuilder();

        if ( !is_null($this->authorizor) ) {
            $this->authorizor
                ->authorizeInsert(
                    $qb->insert($this->table),
                    $fields
                )
                ->execute();

            return intval($qb
                ->getConnection()
                ->lastInsertId());

        } else {
            $values = [];
            $params = [];

            foreach ($fields as $f => $v) {
                $values[$f] = ":$f";
                $params[":$f"] = $v;
            }

            $qb->insert($this->table)
                ->values( $values )
                ->setParameters( $params )
                ->execute();

            // Return the last insert id
            return intval($qb
                ->getConnection()
                ->lastInsertId());
        }
    }


    public function deleteObject($id) {
        $qb = $this->getDB()->createQueryBuilder();

        if ( !is_null($this->authorizor) ) {
            return $this->authorizor
                ->authorizeDelete(
                    $qb->delete($this->table)
                )
                ->where("id = :id")
                ->setParameter(":id", $id)
                ->execute() === 1;
        } else {
            return $qb->delete($this->table)
                ->where("id = :id")
                ->setParameter(":id", $id)
                ->execute() === 1;
        }
    }
}

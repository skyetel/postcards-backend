<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

class SmtpController extends DataObjectController {
    const TABLE = "smtp";

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Set the DAO table
        $this->table = self::TABLE;

        $this->validator = [
            "sender_email" => v::key('sender_email', v::email()->setTemplate("Email must be a valid email address"))->setTemplate("Sender Email must be provided"),
            "host" => v::key('host', v::stringType()->length(1, 255)->setTemplate("Host must be less than 255 in length"), true),
            "username" => v::key('username', v::stringType()->length(1, 255)->setTemplate("Username must be less than 255 in length"), true),
            "password" => v::key('password', v::stringType()->length(null, 255)->setTemplate("Password must be less than 255 in length"), true),
            "port" => v::key('port', v::stringType()->length(1, 255)->setTemplate("Port must be less than 255 in length"), true),
        ];
    }

    private function filterValidateFields( $fields = [] ) {
        $data_fields = $fields;

        foreach ( $fields as $k => $v ) {
            if ( !in_array($k, array_keys($this->validator)) ) {
                unset($data_fields[$k]);
            }
        }

        return $data_fields;
    }

    private function validateUpdate( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        // Assert only the fields that exist
        foreach( array_keys($data_fields) as $k ) {
            if ( $k === "id" )
                continue;

            $this->validator[$k]->assert($data_fields);
        }

        return $data_fields;
    }

    private function validateInsert( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        foreach ( $this->validator as $v ) {
            $v->assert($data_fields);
        }

        return $data_fields;
    }

    public function manageObject (Request $request, Response $response, array $args) {
        if ($request->isGet()) {
            $objects = $this->getObjects();
            return count($objects) > 0 ? $objects[0] : [];
        }
        else if ($request->isPost()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            $objects = $this->getObjects();
            $smtp_exists = count($objects) > 0;

            try {
                if ($smtp_exists) {
                    $this->setObject(null, $fields);
                }
                else {
                    $this->createObject($fields);
                }
            }
            catch(\Exception $e){
                echo $e->getMessage();
            }
        }

        // Shouldn't be here
        throw new \Exception("Unknown request");
    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        return array_map(array($this, "removeSecureFields"), parent::getObjects($offset, $limit, $filters) );
    }

    public function getSettings() {
        $objects = parent::getObjects();
        return count($objects) > 0 ? $objects[0] : [];
    }

    /**
     * Especially remove password on users
     */
    private function removeSecureFields($o) {
        if (is_array($o) ) {
            unset($o["password"]);
        }

        return $o;
    }

    public function setObject($id, $fields=[]) {
        $qb = $this->getDB()->createQueryBuilder();
        $fields = $this->validateUpdate($fields);

        if(empty($fields['password'])){
            unset($fields['password']);
        }

        $values = [];
        $params = [];

        foreach ($fields as $f => $v) {
            $values[$f] = ":$f";
            $params[":$f"] = $v;
        }

        $qb->update($this->table);

        foreach ($fields as $f => $v) {
            $qb->set($f, ":$f")
                ->setParameter(":$f", $v);
        }

        return $qb->execute() === 0;
    }

    public function createObject($fields=[]) {
        $qb = $this->getDB()->createQueryBuilder();
        $fields = $this->validateInsert($fields);

        $values = [];
        $params = [];

        foreach ($fields as $f => $v) {
            $values[$f] = ":$f";
            $params[":$f"] = $v;
        }

        $qb->insert($this->table)
            ->values( $values )
            ->setParameters( $params )
            ->execute();
    }
}

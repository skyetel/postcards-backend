<?php
namespace App\Controllers;

use App\Auth\TemplatesAuthorizer;
use App\Auth\UserIdUsersAuthorizor;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

use \App\Integrations\SkyetelSMSIntegration;

use \App\Auth\UserIdAnnouncementsAuthorizor;
use \App\Auth\UserIdAttachmentsAuthorizor;
use \App\Auth\UserIdPhonenumbersAuthorizor;
use \App\Auth\UserIdDialogsAuthorizor;
use \App\Auth\UserIdMessagesAuthorizor;
use \App\Auth\UserIdContactsAuthorizor;
use \App\Auth\UserIdTemplatesAuthorizor;
use \App\Auth\PublicAttachmentsAuthorizor;

use \App\Helpers\FileHelper;
use \App\Helpers\QueryParamHelper;
use \App\Helpers\StatementFilter;

use \App\Exceptions\AttachmentUnavailableException;

class APIController {
    private $app;
    private $c;

    private $sendHandler;

    public function __construct( $app ) {
        $this->app = $app;
        $this->c = $app->getContainer();

        // Create an integration connection
        $this->sendHandler = new SkyetelSMSIntegration(
            $this->c->skyetelIntegration["sms_url"],
            $this->c->skyetelIntegration["api_sid"],
            $this->c->skyetelIntegration["api_secret"]
        );

        $this->app->map(["POST"], '/login', array($this, 'login'))
            ->setName('api-login');

        $this->app->map(["POST"], '/logout', array($this, 'logout'))
            ->setName('api-logout');

        $this->app->map( ["POST"], '/out', array($this, 'sendMessage'))
            ->setName('api-send');

        $this->app->map( ["POST"], '/in', array($this, 'receiveMessage'))
            ->setName('api-receive');

        $this->app->map( ["GET","POST"], '/poll/messages', array($this, 'pollMessages'))
            ->setName('api-poll-messages');

        $this->app->map( ["GET"], '/messages[/{id}]', array($this, 'getMessages'))
            ->setName('api-messages');

        $this->app->map( ["GET"], '/dialogs[/{id}]', array($this, 'getDialogs'))
            ->setName('api-dialogs');

        $this->app->map( ["GET"], '/phonenumbers[/{id}]', array($this, 'getPhonenumbers'))
            ->setName('api-dialogs');

        $this->app->map( ["GET"], '/dialogs/{id}/messages', array($this, 'getDialogMessages'))
            ->setName('api-dialog-messages');

        // Contact handling
        $this->app->get('/contacts[/{id}]', array($this, 'getContacts'));
        $this->app->post('/contacts[/]', array($this, 'addContact'));
        $this->app->post('/contacts/bulk', array($this, 'bulkAddContact'));
        $this->app->patch('/contacts/{id}', array($this, 'updateContact'));
        $this->app->delete('/contacts/{id}', array($this, 'deleteContact'));

        $this->app->get('/templates[/{id}]', array($this, 'getTemplates'));
        $this->app->post('/templates[/]', array($this, 'addTemplate'));
        $this->app->patch('/templates/{id}', array($this, 'updateTemplate'));
        $this->app->delete('/templates/{id}', array($this, 'deleteTemplate'));

        $this->app->get('/announcements[/{id}]', array($this, 'getAnnouncements'));
        $this->app->post('/announcements[/]', array($this, 'addAnnouncement'));
        $this->app->delete('/announcements/{id}', array($this, 'deleteAnnouncement'));

        $this->app->map( ["GET"], '/attachments/{hash}', array($this, 'getAttachment'))
            ->setName('api-attachments');

        $this->app->patch('/users/{id}', array($this, 'updateUser'));

    }

    public function login (Request $request, Response $response, array $args) {
        if ( isset($_SERVER["PHP_AUTH_USER"]) && isset($_SERVER["PHP_AUTH_PW"])) {
            // Find the user password hash
            $user = $this->c->db->createQueryBuilder()
                ->select("id, password")
                ->from("users")
                ->where("active IS NOT NULL")
                ->andWhere("email = :username")
                ->setParameter(":username", $_SERVER["PHP_AUTH_USER"])
                ->execute()
                ->fetch();

            // Validate the password against the password hash
            if ( is_array($user) && array_key_exists("password", $user)
                && $this->c->passwordEncoder->validatePassword($_SERVER["PHP_AUTH_PW"], $user["password"]) ) {

                    // Generate random token
                    $token = $this->c->generateToken;

                    // Store the token with an expiration
                    $result = $this->c->db->createQueryBuilder()
                        ->update("users")
                        ->where("id = :id")
                        ->setParameter(":id", $user["id"])
                        ->set("login_token_expiration", APIAuthenticationController::LOGIN_EXPIRATION)
                        ->set("login_token", ":token")
                        ->setParameter(":token", $token)
                        ->execute();

                    $filter = [ "and" => [ "id" => [ "eq" => intval($user["id"]) ] ] ];

                    $users = new UsersController($this->app);

                    $json_user = $users->getObject(intval($user["id"]));

                    // Succeed with a token response (if succeeded an update)
                    if ($result == true) {
                        return $response
                            ->withStatus(200)
                            ->withJson( [ "token" => $token, "user" => $json_user ] );
                    }

                }
        }

        // Login attempt that failed so announce Basic auth request
        return $response
            ->withStatus(401)
            ->withHeader('WWW-Authenticate', sprintf('Basic realm="%s"', "SMS/MMS API"));
    }

    public function logout (Request $request, Response $response, array $args) {
        $id = intval($request->getAttribute("users.id"));

        // Clear tokens
        $result = $this->c->db->createQueryBuilder()
            ->update("users")
            ->where("id = :id")
            ->setParameter(":id", $id)
            ->set("login_token_expiration", "NULL")
            ->set("login_token", "NULL")
            ->execute();

        return $response
            ->withStatus(200);
    }

    function getAttachment (Request $request, Response $response, array $args) {
        if ( !empty($request->getAttribute("users.id")) )  {
            $authorizer = new UserIdAttachmentsAuthorizor($request->getAttribute("users.id"));
        } else {
            $authorizor = new PublicAttachmentsAuthorizor();
        }

        $attachments = new AttachmentsController($this->app, $authorizor);

        $matching_file = $attachments->getObjects(0, 1,
            [ "and" => [ "hash" => [ "eq" => $args["hash"] ] ] ]
        );
        

        if (count($matching_file) === 1) {
            $hash = "/var/www/attachments/" . $matching_file[0]["hash"];
            $file = $matching_file[0]["file_name"];

            $response = $response->withHeader('Content-Description', 'File Transfer')
                ->withHeader('Content-Type', 'application/octet-stream')
                ->withHeader('Content-Disposition', 'attachment;filename="'.$file.'"')
                ->withHeader('Expires', '0')
                ->withHeader('Cache-Control', 'must-revalidate')
                ->withHeader('Pragma', 'public')
                ->withHeader('Content-Length', filesize($hash));

            readfile($hash);
            return $response;
        } else {
            // Not found
            return $response
                ->withStatus(404);
        }
    }

    function sendMessage (Request $request, Response $response, array $args) {
        $fields = $request->getParsedBody();

        // If the whole thing was submit as form data (for file upload then convert fields)
        if ( is_array($fields) && array_key_exists("message", $fields) ) {
            if (!is_array($fields["message"]) ) {
                $fields = json_decode($fields["message"], true);
            } else {
                $fields = $fields["message"];
            }
        }

        // Sanity check
        if ( !is_array($fields)
            || !array_key_exists("to", $fields)
            || !array_key_exists("from", $fields)
            || !array_key_exists("text", $fields)
        ) {
            return $response
                ->withStatus(400); // Bad request
        }

        $user_id = $request->getAttribute("users.id");
        $new_message = [];

        $phonenumbers = new PhonenumbersController(
            $this->app,
            new UserIdPhonenumbersAuthorizor($user_id)
        );

        // Give up here if the from phonenumber does not belong to the user or does not exist
        if (empty($phonenumbers->getObjects(0,1, [ "and" => [ "number" => [ "eq" => $fields["from"] ] ] ]) ) ) {
                return $response
                    ->withStatus(403); // Forbidden
        }

        $dialogs = new DialogsController(
            $this->app,
            new UserIdDialogsAuthorizor($user_id)
        );

        $messages = new MessagesController(
            $this->app,
            new UserIdMessagesAuthorizor($user_id)
        );

        $attachments = new AttachmentsController(
            $this->app,
            new UserIdAttachmentsAuthorizor($user_id)
        );

        // Obtain or create a Dialog
        $i = 0;
        do {
            $exists = $dialogs->getObjects(0, 1,
                [ "and" =>
                    [
                        "internal_number" => [ "eq" => $fields["from"] ],
                        "external_number" => [ "eq" => $fields["to"] ]
                    ]
                ]
            );

            if (count($exists) === 1) {
                $new_message["dialog_id"] = $exists[0]["id"];
                break;
            } else {
                $dialogs->createObject(
                    [
                        "internal_number" => $fields["from"],
                        "external_number" => $fields["to"]
                    ]
                );
            }
        } while($i++ < 2);

        // This shouldn't happen but protect from it
        if ( ! array_key_exists("dialog_id", $new_message) ) {
            throw new \Exception("Could not create a dialog");
        }

        $message_id = $messages->createObject(
            array_merge(
                $new_message,
                [
                    "sent" => (new \Datetime())->format("Y-m-d H:i:s"),
                    "direction" => MessagesController::OUT_DIRECTION,
                    "message" => $fields["text"]
                ]
            )
        );

        // Handle attachments if any
        $media = [];
        $files = $request->getUploadedFiles();

        if (array_key_exists("media", $files)) {
            foreach ( $files["media"] as $uploadedFile ) {
                if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                    $file_name = $uploadedFile->getClientFilename();
                    $hash = FileHelper::moveUploadedFile($uploadedFile);

                    $attachments->createObject(
                        [
                            "message_id" => $message_id,
                            "file_name" => $file_name,
                            "hash" => $hash,
                            "public_expiration" => (new \Datetime('+20 minutes'))->format("Y-m-d H:i:s")
                        ]
                    );

                    $media["media"][] = $this->c->attachmentURL . $hash;
                }

            }
        }

        // Handle sending the message
        $send_message = array_merge(
            [
                "to" => $fields["to"],
                "text" => $fields["text"]
            ],
            $media
        );

        try {
            $result = $this->sendHandler
                ->send($fields["from"],
                    $send_message
                );

        } catch (\Exception $e) {
            return $response
                ->withStatus(500)
                ->withJson( [ "ERROR" => $e->getMessage() ]);
        }

        return $response
            ->withJson( $messages->getObject(intval($message_id)) );
    }

    function receiveMessage (Request $request, Response $response, array $args) {
        $fields = $request->getParsedBody();

        // Sanity check
        if ( !is_array($fields)
            || !array_key_exists("to", $fields)
            || !array_key_exists("from", $fields)
            || !array_key_exists("text", $fields)
        ) {
            return $response
                ->withStatus(400); // Bad request
        }

        // Sanitize the TO/FROM fields to be safe
        $to = preg_replace('/[^0-9]/', '', $fields["to"]);
        $from = preg_replace('/[^0-9]/', '', $fields["from"]);


        $phonenumbers = new PhonenumbersController($this->app);

        $phonenumber_match = $phonenumbers->getObjects(0,1, [ "and" => [ "number" => [ "eq" => $to ] ] ]);

        // Give up here if the from phonenumber does not exist or is not assigned to a user
        if ( is_array($phonenumber_match) ) {
            $phonenumber = $phonenumber_match[0];

            if ( is_array($phonenumber)
                && array_key_exists("user", $phonenumber)
                && is_array($phonenumber["user"])
                && array_key_exists("id", $phonenumber["user"])
                && is_numeric($phonenumber["user"]["id"])
            ) {

                // Save the user associated with phonenumber
                $user = $phonenumber["user"];
            } else {
                return $response
                    ->withStatus(403); // Forbidden
            }
        } else {
            return $response
                ->withStatus(403); // Forbidden
        }

        $dialogs = new DialogsController($this->app);
        $messages = new MessagesController($this->app);
        $attachments = new AttachmentsController($this->app);
        $smtp = new SmtpController($this->app);

        // Obtain or create a Dialog
        $new_message = [];
        $i = 0;
        do {
            $exists = $dialogs->getObjects(0, 1,
                [ "and" =>
                    [
                        "internal_number" => [ "eq" => $to ],
                        "external_number" => [ "eq" => $from ],
                        "user_id" => [ "eq" => intval($user["id"]) ]
                    ]
                ]
            );

            if (count($exists) === 1) {
                $dialog_id = $exists[0]["id"];
                $exists[0]['archived'] = 0;
                $dialogs->setObject( $dialog_id, $exists[0]);

                $new_message["dialog_id"] = $dialog_id;
                break;
            } else {
                $dialogs->createObject(
                    [
                        "internal_number" => $to,
                        "external_number" => $from,
                        "user_id" => intval($user["id"])
                    ]
                );
            }
        } while($i++ < 2);

        // This shouldn't happen but protect from it
        if ( ! array_key_exists("dialog_id", $new_message) ) {
            throw new \Exception("Could not create a dialog");
        }

        $message_id = $messages->createObject(
            array_merge(
                $new_message,
                [
                    "sent" => (new \Datetime())->format("Y-m-d H:i:s"),
                    "direction" => MessagesController::IN_DIRECTION,
                    "message" => $fields["text"]
                ]
            )
        );

        $this->sendAutoReply($messages, $new_message, $from, $to);

        $smtp_settings = $smtp->getSettings();

        if($user['notifications_enabled'] && !empty($smtp_settings)){
            $this->sendEmailNotification($smtp_settings, $user['email'], $from);
        }

        // Handle attachments if any (URLs expected)
        if (array_key_exists("media", $fields) && is_array($fields["media"]) ) {
            foreach ( $fields["media"] as $url ) {
                if ( filter_var($url, FILTER_VALIDATE_URL) ) {

                    // Get filename from URL
                    $file_name = explode("?", basename($url))[0];

                    try {
                        // Download URL and get the resulting hash
                        $hash = FileHelper::downloadURLtoFileHash($url);

                        // Create the corresponding data record for Attachment to new message
                        $attachments->createObject(
                            [
                                "message_id" => $message_id,
                                "file_name" => $file_name,
                                "hash" => $hash,
                                "public_expiration" => (new \Datetime())->format("Y-m-d H:i:s")
                            ]
                        );
                    } catch (AttachmentUnavailableException $e) {
                        // skip and continue if unable to download
                        continue;
                    }
                }

            }
        }

        // Success created
        return $response
            ->withStatus( 201 );
    }

    private function sendEmailNotification($smtp_info, $email, $from){
        try {
            $mail = new PHPMailer();

            $mail->isSMTP();
            $mail->isHTML();
            $mail->Host = $smtp_info['host'];
            $mail->Username = $smtp_info['username'];
            $mail->Password = $smtp_info['password'];
            $mail->Port = $smtp_info['port'];
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

            $mail->setFrom($smtp_info['sender_email']);
            $mail->addAddress($email);

            $mail->Subject = 'New Notification';
            $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];

            $mail->Body = '<p>You have received a new message from ' . $from . '</p>'  .  '<p>View it here: ' . $url . ' </p>';

            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            }
        }
        catch(\Exception $e){
            echo $e->getMessage();
        }
    }

    private function sendAutoReply($messages, $new_message, $from, $to){
        try {
            sleep(1);
            $contacts = new ContactsController($this->app);

            $contact = $contacts->getObjects(0, 1,
                [ "and" =>
                    [
                        "number" => [ "eq" => $from ]
                    ]
                ]
            );

            if(count($contact) === 1 && !empty($contact[0]['auto_reply'])){
                $auto_reply = $contact[0]['auto_reply'];

                $messages->createObject(
                    array_merge(
                        $new_message,
                        [
                            "sent" => (new \Datetime())->format("Y-m-d H:i:s"),
                            "direction" => MessagesController::OUT_DIRECTION,
                            "message" => $auto_reply
                        ]
                    )
                );

                $send_message = ["to" => $from, "text" => $auto_reply];

                $this->sendHandler
                    ->send($to,
                        $send_message
                    );
            }
        }
        catch (\Exception $e) {

        }
    }

    function pollMessages (Request $request, Response $response, array $args) {

        if ($request->isGet()) {

            $query = $request->getQueryParams();
            
            $messages = new MessagesController(
                $this->app,
                new UserIdMessagesAuthorizor($request->getAttribute("users.id"))
            );


            $filter = QueryParamHelper::getFilterParam( $query );
            $dialog_filter = [ "and" => [ "viewed" => [ "eq" => MessagesController::NOT_VIEWED ] ] ];

            return $response
                ->withJson(
                    $messages->getObjects(
                        QueryParamHelper::getOffsetParam( $query ),
                        QueryParamHelper::getLimitParam( $query ),
                        StatementFilter::mergeFilters($filter, $dialog_filter),
                        array_merge(
                            QueryParamHelper::getSortParam( $query ),
                            [ "-sent" ] // Latest sent orderd first
                        )

                    )
                );

        } else if ($request->isPost()) {
            $fields = $request->getParsedBody();

            if (is_array($fields) ) {
                $messages = new MessagesController(
                    $this->app,
                    new UserIdMessagesAuthorizor($request->getAttribute("users.id"))
                );

                foreach ($fields as $id => $object ) {

                    // Technically this could update multiple fields
                    // However, this is only intended to update "viewed" field
                    $messages->setObject($id, $object);

                }

            }

            return $response
                ->withStatus(200);
        }

        return $response
            ->withStatus(400);
    }

    function getMessages (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $messages = new MessagesController(
            $this->app,
            new UserIdMessagesAuthorizor($request->getAttribute("users.id"))
        );

        if (array_key_exists("id", $args) ) {
            $retval = $messages->getObject(intval($args["id"]));

            if ($retval === false) {
                return $response
                    ->withStatus(404);
            } else {
                return $response
                    ->withJson($retval);
            }
        } else {
            return $response
                ->withJson(
                    $messages->getObjects(
                        QueryParamHelper::getOffsetParam( $query ),
                        QueryParamHelper::getLimitParam( $query ),
                        QueryParamHelper::getFilterParam( $query ),
                        QueryParamHelper::getSortParam( $query )
                    )
                );
        }
    }

    function getDialogs (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $dialogs = new DialogsController(
            $this->app,
            new UserIdDialogsAuthorizor($request->getAttribute("users.id"))
        );

        if (array_key_exists("id", $args) ) {
            $retval = $dialogs->getObject(intval($args["id"]));

            if ($retval === false) {
                return $response
                    ->withStatus(404);
            } else {
                return $response
                    ->withJson($retval);
            }
        } else {
            $filter = QueryParamHelper::getFilterParam( $query );

            if(array_key_exists('and', $filter) &&
                array_key_exists('archived', $filter['and']) &&
                array_key_exists('like', $filter['and']['archived'])){
                $filter['and']['archived']['like'] = (int) filter_var($filter['and']['archived']['like'], FILTER_VALIDATE_BOOLEAN);
            }

            return $response
                ->withJson(
                    $dialogs->getObjects(
                        QueryParamHelper::getOffsetParam( $query ),
                        QueryParamHelper::getLimitParam( $query ),
                        $filter,
                        QueryParamHelper::getSortParam( $query )
                    )
                );
        }
    }

    function getPhonenumbers (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $phonenumbers = new PhonenumbersController(
            $this->app,
            new UserIdPhonenumbersAuthorizor($request->getAttribute("users.id"))
        );

        if (array_key_exists("id", $args) ) {
            $retval = $phonenumbers->getObject(intval($args["id"]));

            if ($retval === false) {
                return $response
                    ->withStatus(404);
            } else {
                return $response
                    ->withJson($retval);
            }
        } else {
            return $response
                ->withJson(
                    $phonenumbers->getObjects(
                        QueryParamHelper::getOffsetParam( $query ),
                        QueryParamHelper::getLimitParam( $query ),
                        QueryParamHelper::getFilterParam( $query ),
                        QueryParamHelper::getSortParam( $query )
                    )
                );
        }
    }

    /**
     *
     */
    function getDialogMessages (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $messages = new MessagesController(
            $this->app,
            new UserIdMessagesAuthorizor($request->getAttribute("users.id"))
        );

        $filter = QueryParamHelper::getFilterParam( $query );
        $dialog_filter = [ "and" => [ "dialog_id" => [ "eq" => intval($args["id"]) ] ] ];

        return $response
            ->withJson(
                $messages->getObjects(
                    QueryParamHelper::getOffsetParam( $query ),
                    QueryParamHelper::getLimitParam( $query ),
                    StatementFilter::mergeFilters($filter, $dialog_filter),
                    QueryParamHelper::getSortParam( $query )
                )
            );
    }

    /**
     * List contacts
     */
    function getContacts (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $contacts = new ContactsController(
            $this->app,
            new UserIdContactsAuthorizor($request->getAttribute("users.id"))
        );

        if (array_key_exists("id", $args)) {
            // Return a single contact
            $contact = $contacts->getObject(intval($args["id"]));

            if ( is_array($contact) && count($contact) > 0 ) {
                return $response->withJson($contact);
            } else {
                return $response->withStatus(404);
            }
        }

        $filter =  QueryParamHelper::getFilterParam($query);

        if(array_key_exists('and', $filter) &&
            array_key_exists('archived', $filter['and']) &&
            array_key_exists('like', $filter['and']['archived'])){
            $filter['and']['archived']['like'] = (int) filter_var($filter['and']['archived']['like'], FILTER_VALIDATE_BOOLEAN);
        }

        return $response
            ->withJson(
                $contacts->getObjects(
                    QueryParamHelper::getOffsetParam( $query ),
                    QueryParamHelper::getLimitParam( $query ),
                    $filter,
                    QueryParamHelper::getSortParam( $query )
                )
            );
    }

    /**
     * Add contact
     */
    function addContact (Request $request, Response $response, array $args) {
        $contacts = new ContactsController(
            $this->app,
            new UserIdContactsAuthorizor($request->getAttribute("users.id"))
        );

        $ret = $contacts->createObject($request->getParsedBody());

        return $response
            ->withJson(
                $contacts->getObject(intval($ret))
            );
    }

    function bulkAddContact (Request $request, Response $response, array $args) {
        $contacts = new ContactsController(
            $this->app,
            new UserIdContactsAuthorizor($request->getAttribute("users.id"))
        );

        $created_contacts = [];

        $new_contacts = $request->getParsedBody();

        foreach($new_contacts as $new_contact){
            try {
                $new_contact_id = $contacts->createObject($new_contact);
                $created_contacts[] = $contacts->getObject($new_contact_id);
            }
            catch(\Exception $e){

            }
        }

        return $response->withJson($created_contacts);
    }

    function updateUser (Request $request, Response $response, array $args) {
        $users = new UsersController(
            $this->app,
            new UserIdUsersAuthorizor($request->getAttribute("users.id"))
        );

        $fields = $request->getParsedBody();
        $user = $users->getObject((int)$args["id"]);

        $user['notifications_enabled'] = array_key_exists('notifications_enabled', $fields) ? $fields['notifications_enabled'] : false;

        $users->setObject((int)$args["id"], $user);

        // Return the re-lookup of the object
        return $response
            ->withJson(
                $users->getObject((int)$args["id"])
            );
    }

    /**
     * Update contact
     */
    function updateContact (Request $request, Response $response, array $args) {
        $contacts = new ContactsController(
            $this->app,
            new UserIdContactsAuthorizor($request->getAttribute("users.id"))
        );

        //throw new \Exception(print_r($request->getParsedBody(), true));
        $contacts->setObject(intval($args["id"]), $request->getParsedBody());

        // Return the re-lookup of the object
        return $response
            ->withJson(
                $contacts->getObject(intval($args["id"]))
            );
    }

    /**
     * Delete contact
     */
    function deleteContact (Request $request, Response $response, array $args) {
        $contacts = new ContactsController(
            $this->app,
            new UserIdContactsAuthorizor($request->getAttribute("users.id"))
        );

        $contacts->deleteObject(intval($args["id"]), $request->getParsedBody());

        return $response
            ->withStatus(200);
    }


    function getTemplates (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $templates = new TemplatesController(
            $this->app,
            new UserIdTemplatesAuthorizor($request->getAttribute("users.id"))
        );

        if (array_key_exists("id", $args)) {
            $template = $templates->getObject(intval($args["id"]));

            if ( is_array($template) && count($template) > 0 ) {
                return $response->withJson($template);
            } else {
                return $response->withStatus(404);
            }
        }

        return $response
            ->withJson(
                $templates->getObjects(
                    QueryParamHelper::getOffsetParam( $query ),
                    QueryParamHelper::getLimitParam( $query ),
                    QueryParamHelper::getFilterParam( $query ),
                    QueryParamHelper::getSortParam( $query )
                )
            );

    }

    function addTemplate (Request $request, Response $response, array $args) {
        $templates = new TemplatesController(
            $this->app,
            new UserIdTemplatesAuthorizor($request->getAttribute("users.id"))
        );

        $ret = $templates->createObject($request->getParsedBody());

        return $response
            ->withJson(
                $templates->getObject($ret)
            );
    }


    function updateTemplate (Request $request, Response $response, array $args) {
        $templates = new TemplatesController(
            $this->app,
            new UserIdTemplatesAuthorizor($request->getAttribute("users.id"))
        );

        //throw new \Exception(print_r($request->getParsedBody(), true));
        $templates->setObject((int)$args["id"], $request->getParsedBody());

        // Return the re-lookup of the object
        return $response
            ->withJson(
                $templates->getObject((int)$args["id"])
            );
    }


    function deleteTemplate (Request $request, Response $response, array $args) {
        $templates = new TemplatesController(
            $this->app,
            new UserIdTemplatesAuthorizor($request->getAttribute("users.id"))
        );

        $templates->deleteObject((int)$args["id"]);

        return $response
            ->withStatus(200);
    }


    function getAnnouncements (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $announcements = new AnnouncementsController(
            $this->app,
            new UserIdAnnouncementsAuthorizor($request->getAttribute("users.id"))
        );

        if (array_key_exists("id", $args)) {
            $announcement = $announcements->getObject((int)$args["id"]);

            if ( is_array($announcement) && count($announcement) > 0 ) {
                return $response->withJson($announcement);
            } else {
                return $response->withStatus(404);
            }
        }

        return $response
            ->withJson(
                $announcements->getObjects(
                    QueryParamHelper::getOffsetParam( $query ),
                    QueryParamHelper::getLimitParam( $query ),
                    QueryParamHelper::getFilterParam( $query ),
                    QueryParamHelper::getSortParam( $query )
                )
            );

    }

    function addAnnouncement (Request $request, Response $response, array $args) {
        $announcements = new AnnouncementsController(
            $this->app,
            new UserIdAnnouncementsAuthorizor($request->getAttribute("users.id"))
        );
        
        $fields = $request->getParsedBody();

        if(count($fields['phonenumbers']) > 50){
            return $response
                ->withStatus(400)
                ->withJson( [ "ERROR" => 'Max 50 phonenumbers' ]);
        }

        $fields['phonenumbers'] = implode(',', array_unique($fields['phonenumbers']));
        $fields['state'] = AnnouncementsController::STATE_SCHEDULED;

        $ret = $announcements->createObject($fields);

        return $response
            ->withJson(
                $announcements->getObject($ret)
            );
    }

    function deleteAnnouncement (Request $request, Response $response, array $args) {
        $announcements = new AnnouncementsController(
            $this->app,
            new UserIdAnnouncementsAuthorizor($request->getAttribute("users.id"))
        );

        $announcement = $announcements->getObject((int)$args["id"]);

        if($announcement['state'] === AnnouncementsController::STATE_SCHEDULED){
            $announcement['state'] = AnnouncementsController::STATE_CANCELED;
            $announcement['phonenumbers'] = implode(',', $announcement['phonenumbers']);
            $announcements->setObject((int)$args["id"], $announcement);
        }

        return $response
            ->withStatus(200);
    }

}

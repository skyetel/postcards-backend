<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

use \App\Auth\UserIdContactsAuthorizor;

class DialogsController extends DataObjectController {
    const TABLE = "dialogs";

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Set the DAO table
        $this->table = self::TABLE;

        // TODO: Move this into a Data Model masked by a Data Model Authorization
        $this->validator = [
            "user_id" => v::key('user_id', v::intVal()->min(1), false ),
            "internal_number" => v::key('internal_number', v::allOf( v::stringType()->length(11, 11), v::startsWith('1') )->setTemplate("Phone Number must be a valid 11-digit number representing a NANPA phonenumber") )->setTemplate("Internal Phone Number must be provided"),
            "external_number" => v::key('external_number', v::allOf( v::stringType()->length(11, 11), v::startsWith('1') )->setTemplate("Phone Number must be a valid 11-digit number representing a NANPA phonenumber") )->setTemplate("External Phone Number must be provided"),
            "archived" => v::key('archived', v::boolVal(), false),
        ];
    }

    private function filterValidateFields( $fields = [] ) {
        $data_fields = $fields;

        foreach ( $fields as $k => $v ) {
            if ( !in_array($k, array_keys($this->validator)) ) {
                unset($data_fields[$k]);
            }
        }

        if(array_key_exists('archived', $data_fields)){
            $data_fields['archived'] = (int) filter_var($data_fields['archived'], FILTER_VALIDATE_BOOLEAN);
        }

        return $data_fields;

        //Works in PHP 5.6
        //return array_filter($fields, function ($key) {
        //        return in_array($key, array_keys($this->validator));
        //    },
        //    ARRAY_FILTER_USE_KEY
        //);
    }


    private function validateUpdate( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        // Only id is required
        v::intVal()->min(1)->assert($fields["id"]);

        // Assert only the fields that exist
        foreach( array_keys($data_fields) as $k ) {
            if ( $k === "id" )
                continue;

            $this->validator[$k]->assert($data_fields);
        }

        return $data_fields;
    }

    private function validateInsert( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        foreach ( $this->validator as $v ) {
            $v->assert($data_fields);
        }

        return $data_fields;
    }

    public function manageObject (Request $request, Response $response, array $args) {

        if ($request->isGet()) {
            $query = $request->getQueryParams();

            if (array_key_exists("id", $args)) {

                return $this->getObject(intval($args["id"]));

            } else {

                return $this->getObjects(
                    $this->getOffsetParam( $query ),
                    $this->getLimitParam( $query ),
                    $this->getFilterParam( $query )
                );

            }

        } else if ($request->isPost()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // NOTE: This is not technically correct to HTTP standard
            // PATCH should be used for an update
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);

            } else {

                return $this->createObject($fields);

            }

        } else if ($request->isPatch()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // Essentially this is the same as POST with an id
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);
            }

        } else if ($request->isDelete()) {
            if (array_key_exists("id", $args)) {

                return $this->deleteObject(intval($args["id"]));

            }
        }

        // Shouldn't be here
        throw new \Exception("Unknown request");
    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        $objects = array_map( array($this, "getRelationships"), parent::getObjects($offset, $limit, $filters) );
        return array_map( array($this, "convertData"), $objects );
    }

    public function getObject($id) {
        $object = $this->getRelationships( parent::getObject($id) );
        return $this->convertData($object);
    }

    private function convertData($o){
        if (array_key_exists("archived", $o)) {
            $o['archived'] = filter_var($o['archived'], FILTER_VALIDATE_BOOLEAN);
        }

        return $o;
    }
    private function getRelationships($o) {
        if (array_key_exists("user_id", $o)) {
            $u = new UsersController($this->getApp());
            $o["user"] = $u->getObject($o["user_id"]);

            $c = new ContactsController(
                $this->getApp(),
                new UserIdContactsAuthorizor(intval($o["user_id"]))
            );

            $contact = array_pop($c->getObjects(
                    0,
                    1,
                    [ "and" => [ "number" => [ "eq" => $o["external_number"] ] ] ] 
                )
            );

            if (is_array($contact) && count($contact) > 0) {
                $o["contact"] = $contact;
            }

            unset($o["user_id"]);
        } else if ( is_array($o) ) {
            $o["user"] = null;
        }
            // Ability to include contact information on external number

        return $o;
    }

    public function setObject($id, $fields=[]) {
        return parent::setObject($id,
            $this->validateUpdate($fields)
        );
    }

    public function createObject($fields=[]) {
        return parent::createObject( $this->validateInsert($fields) );
    }

    public function deleteObject($id) {
        return parent::deleteObject($id);
    }
}

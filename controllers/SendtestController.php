<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

use \App\Integrations\SkyetelSMSIntegration;
use \App\Helpers\FileHelper;

class SendtestController extends DataObjectController {
    const TABLE = "phonenumbers";

    private $sendHandler;

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Create an integration connection
        $this->sendHandler = new SkyetelSMSIntegration(
            $this->getContainer()->skyetelIntegration["sms_url"],
            $this->getContainer()->skyetelIntegration["api_sid"],
            $this->getContainer()->skyetelIntegration["api_secret"]
        );

        // Set the DAO table
        $this->table = self::TABLE;
    }

    public function manageObject (Request $request, Response $response, array $args) {
        $exception = [];
        $fields = [];

        if ($request->isPost()) {
            $media = [];
            $fields = $this->getDataFields( $request->getParsedBody() );

            // Handle uploaded files
            $files = $request->getUploadedFiles();

            if ( array_key_exists("data", $files) && array_key_exists("upload_file", $files["data"]) ) {

                $attachments = new AttachmentsController($this->getApp());

                $media["media"] = [];

                foreach ( $files["data"]["upload_file"] as $uploadedFile ) {
                    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                        $file_name = $uploadedFile->getClientFilename();
                        $hash = FileHelper::moveUploadedFile($uploadedFile);

                        $attachments->createObject(
                            [
                                "file_name" => $file_name,
                                "hash" => $hash,
                                "public_expiration" => (new \Datetime('+20 minutes'))->format("Y-m-d H:i:s")
                            ]
                        );

                        $media["media"][] = $this->getContainer()->attachmentURL . $hash;
                    }
                }
            }


            // Handle sending the message
            try {
                $result = $this->sendHandler
                    ->send($fields["from"],
                        array_merge(
                            [
                                "to" => $fields["to"],
                                "text" => $fields["text"]
                            ],
                            $media
                        )
                    );

                $fields["message"] = "Message sent";
            } catch (\Exception $e) {
                // Let exception be returned to Admin without being thrown so data can still be passed
                $exception = [ "exception" => $e ];
            }
                        
        }

        $query = $request->getQueryParams();

        return array_merge([ 
                "phonenumbers" => $this->getObjects(
                    $this->getOffsetParam( $query ),
                    $this->getLimitParam( $query ),
                    $this->getFilterParam( $query )
                )
            ],
            $exception,
            $fields
        );

    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        return array_map( array($this, "getUserRelationship"), parent::getObjects($offset, $limit, $filters) );
    }

    public function getObject($id) {
        return $this->getUserRelationship( parent::getObject($id) );
    }

    private function getUserRelationship($p) {
        if (array_key_exists("user_id", $p)) {
            $u = new UsersController($this->getApp(), $this->getAuthorizor());
            $p["user"] = $u->getObject($p["user_id"]);

            unset($p["user_id"]);
        } else if ( is_array($p) ) {
            $p["user"] = null;
        }

        return $p;
    }

    public function setObject($id, $fields=[]) {
        throw new \Exception("Not implemented");
    }

    public function createObject($fields=[]) {
        throw new \Exception("Not implemented");
    }

    public function deleteObject($id) {
        throw new \Exception("Not implemented");
    }
}

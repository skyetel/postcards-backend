<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

class UsersController extends DataObjectController {
    const TABLE = "users";
    const VALIDATOR = "users";

    protected $smtp_controller;

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Set the DAO table
        $this->table = self::TABLE;

        $this->smtp_controller = new SmtpController($app);

        // TODO: Move this into a Data Model masked by a Data Model Authorization
        $this->validator = [
            "email" => v::key('email', v::email()->setTemplate("Email must be a valid email address"))->setTemplate("Email must be provided"),
            "display_name" => v::key('display_name', v::stringType()->length(null, 255)->setTemplate("Display Name must be less than 255 in length"), false),
            "password" => v::key('password', v::stringType()->length(8, null)->setTemplate("Password must be at least 8 characters") )->setTemplate("Password must be provided"),
            "notifications_enabled" =>  v::key('notifications_enabled', v::boolVal(), false),
        ];
    }

    private function filterValidateFields( $fields = [] ) {
        $data_fields = $fields;

        foreach ( $fields as $k => $v ) {
            if ( !in_array($k, array_keys($this->validator)) ) {
                unset($data_fields[$k]);
            }
        }

        if(array_key_exists('notifications_enabled', $data_fields)){
            $data_fields['notifications_enabled'] = (int) filter_var($data_fields['notifications_enabled'], FILTER_VALIDATE_BOOLEAN);
        }

        return $data_fields;

        //Works in PHP 5.6
        //return array_filter($fields, function ($key) {
        //        return in_array($key, array_keys($this->validator));
        //    },
        //    ARRAY_FILTER_USE_KEY
        //);
    }

    private function processPassword( $password ) {
        return $this->getContainer()->passwordEncoder->getPasswordData($password);
    }

    private function validateUpdate( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        // Helpful if needed for removing empty fields: array_filter($fields, function ($v) { return $v !== ""; });
        
        // Remove password if it is empty
        if ( array_key_exists("password", $data_fields) && $data_fields["password"] === "") {
            unset($data_fields["password"]);
        }

        if(!array_key_exists('notifications_enabled', $data_fields)){
            $data_fields['notifications_enabled'] = 0;
        }
        
        // Only id is required
        v::intVal()->min(1)->assert($fields["id"]);

        // Assert only the fields that exist
        foreach( array_keys($data_fields) as $k ) {
            if ( $k === "id" )
                continue;

            $this->validator[$k]->assert($data_fields);
        }

        // Special requirement for password if it is provided
        if ( array_key_exists("password", $data_fields) ) {
            $data_fields["password"] = $this->processPassword($data_fields["password"]);
        }

        return $data_fields;
    }

    private function validateInsert( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        foreach ( $this->validator as $v ) {
            $v->assert($data_fields);
        }

        // Special requirement for password if it is provided
        $data_fields["password"] = $this->processPassword($data_fields["password"]);

        return $data_fields;
    }

    public function manageObject (Request $request, Response $response, array $args) {

        if ($request->isGet()) {
            $query = $request->getQueryParams();

            if (array_key_exists("id", $args)) {

                return $this->getObject(intval($args["id"]));

            } else {

                return $this->getObjects(
                    $this->getOffsetParam( $query ),
                    $this->getLimitParam( $query ),
                    $this->getFilterParam( $query )
                );

            }

        } else if ($request->isPost()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // NOTE: This is not technically correct to HTTP standard
            // PATCH should be used for an update
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);

            } else {

                return $this->createObject($fields);

            }

        } else if ($request->isPatch()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // Essentially this is the same as POST with an id
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);
            }

        } else if ($request->isDelete()) {
            if (array_key_exists("id", $args)) {

                return $this->deleteObject(intval($args["id"]));

            }
        }

        // Shouldn't be here
        throw new \Exception("Unknown request");
    }

    public function getSecureObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        return parent::getObjects($offset, $limit, $filters);
    }

    public function getSecureObject($id) {
        return parent::getObject($id);
    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        $objects = array_map(array($this, "removeSecureFields"), parent::getObjects($offset, $limit, $filters) );
        return array_map( array($this, "convertData"), $objects );
    }

    public function getObject($id) {
        $object = $this->removeSecureFields( parent::getObject($id) );
        return $this->convertData($object);
    }

    private function convertData($o){
        if (is_array($o) && array_key_exists("notifications_enabled", $o)) {
            $o['notifications_enabled'] = filter_var($o['notifications_enabled'], FILTER_VALIDATE_BOOLEAN);

            $smtp_enabled = $this->smtp_controller->getObjects();
            $o['notifications_available'] = count($smtp_enabled) > 0;
        }

        return $o;
    }

    /**
     * Especially remove password on users
     */
    private function removeSecureFields($o) {
        if (is_array($o) ) {
            unset($o["password"]);
            unset($o["login_token"]);
            unset($o["login_token_expiration"]);
            unset($o["active"]);
        }

        return $o;
    }

    public function getUserPhoneNumbers($id, $offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        $p = new PhoneNumberController($this->getDB());

        $filters = array_merge($filters, ["user_id" => $id]);

        return $p->getPhoneNumbers($offset, $limit, $filters);
    }

    public function setObject($id, $fields=[]) {
        return parent::setObject($id,
            $this->validateUpdate($fields)
        );
    }

    public function createObject($fields=[]) {
        return parent::createObject( $this->validateInsert($fields) );
    }

    public function deleteObject($id) {
        return parent::deleteObject($id);
    }
}

<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

use \App\Helpers\FileHelper;

class MessagesController extends DataObjectController {
    const TABLE = "messages";

    const IN_DIRECTION  = 0;
    const OUT_DIRECTION = 1;

    const NOT_VIEWED    = 0;
    const VIEWED        = 1;

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Set the DAO table
        $this->table = self::TABLE;

        // TODO: Move this into a Data Model masked by a Data Model Authorization
        $this->validator = [
            "user_id" => v::key('user_id', v::oneOf( v::equals(''), v::nullType(), v::intVal()->min(1) ) )
        ];
    }

    private function filterValidateFields( $fields = [] ) {
        $data_fields = $fields;

        foreach ( $fields as $k => $v ) {
            if ( !in_array($k, array_keys($this->validator)) ) {
                unset($data_fields[$k]);
            }
        }

        return $data_fields;

        //Works in PHP 5.6
        //return array_filter($fields, function ($key) {
        //        return in_array($key, array_keys($this->validator));
        //    },
        //    ARRAY_FILTER_USE_KEY
        //);
    }



    private function validateInsert( $fields = [] ) {
        throw new \Exception("Not implemented");
        return null;
    }

    public function manageObject (Request $request, Response $response, array $args) {

        if ($request->isPost()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // NOTE: This is not technically correct to HTTP standard
            // PATCH should be used for an update
            if (array_key_exists("id", $fields)) {

                $this->setObject(intval($fields["id"]), $fields);
            }

        } else if ($request->isPatch()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // Essentially this is the same as POST with an id
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);
            }

        } else if ($request->isDelete()) {
            if (array_key_exists("id", $args)) {
                // Delete just means unassign
                $this->setObject(intval($args["id"]), [ "user_id" => null ] );
            }
        }
            
        // Always return results
        $query = $request->getQueryParams();

        $u = new UsersController($this->getApp(), $this->getAuthorizor());

        // fudge the offsets, limits, and filters
        if ( array_key_exists("phonenumbers", $query) && is_array($query["phonenumbers"]) ) {
            $phonenumbers_query = $query["phonenumbers"];
        } else {
            $phonenumbers_query = [];
        }


        if ( array_key_exists("users", $query) && is_array($query["users"]) ) {
            $users_query = $query["users"];
        } else {
            $users_query = [];
        }

        return [ 
            "phonenumbers" => $this->getObjects(
                $this->getOffsetParam( $phonenumbers_query ),
                $this->getLimitParam( $phonenumbers_query ),
                $this->getFilterParam( $phonenumbers_query )
            ),
            "users" => $u->getObjects(
                $this->getOffsetParam( $users_query ),
                $this->getLimitParam( $users_query ),
                $this->getFilterParam( $users_query )
            )
        ];
    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = [], $sort = []) {
        return array_map( array($this, "getRelationships"), parent::getObjects($offset, $limit, $filters, $sort) );
    }

    public function getObject($id) {
        return $this->getRelationships( parent::getObject($id) );
    }

    private function getRelationships($o) {
        if (is_array($o) && array_key_exists("dialog_id", $o)) {
            $d = new DialogsController($this->getApp());
            $a = new AttachmentsController($this->getApp());

            $dialog = $d->getObject(intval($o["dialog_id"]));
            $attachment_list = $a->getObjects(0, 10,
                [ "and" => [ "message_id" => [ "eq" => intval($o["id"]) ] ] ]
            );

            // Extract and determine Attachment URLs
            if ( is_array($attachment_list) ) {
                $container = $this->getContainer();
                $o["media"] = array_map(function($att) use ($container) {
                        return $container->attachmentURL . $att["hash"];
                    },
                    $attachment_list
                );

                // Cherry pick any image files
                $image_attachments = array_filter($attachment_list,
                    function($att) {
                        return FileHelper::getImageDimensions($att["hash"]) !== false;
                    }
                );

                // Look up dimensions of images
                $o["media_hint"] = [];
                foreach($image_attachments as $v) {
                    $o["media_hint"][$this->getContainer()->attachmentURL . $v["hash"]] = FileHelper::getImageDimensions($v["hash"]);
                }
            } 

            $o["dialog"] = $dialog;

        }

        return $o;
    }

    public function setObject($id, $fields=[]) {
        // Just to be safe we will only allow updating "viewed" field on messages
        if ( array_key_exists("viewed", $fields) ) {
            $allowed_fields = [ "viewed" => $fields["viewed"] ];
        }

        return parent::setObject( $id, $allowed_fields );
    }

    public function createObject($fields=[]) {
        return parent::createObject( $fields );
    }

    public function deleteObject($id) {
        throw new \Exception("Not implemented");
    }
}


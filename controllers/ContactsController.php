<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

class ContactsController extends DataObjectController {
    const TABLE = "contacts";

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Set the DAO table
        $this->table = self::TABLE;

        // TODO: Move this into a Data Model masked by a Data Model Authorization
        $this->validator = [
            "number" => v::key('number', v::allOf( v::stringType()->length(11, 11), v::startsWith('1') )->setTemplate("Phone Number must be a valid 11-digit number representing a NANPA phonenumber") )->setTemplate("Internal Phone Number must be provided"),
            "display_name" => v::key('display_name', v::stringType()->length(null, 255)->setTemplate("Must be a valid display name no longer than 255 characters"), false),
            "archived" => v::key('archived', v::boolVal(), false),
            "auto_reply" => v::key('auto_reply', v::optional(v::stringType()->length(null, 99999)), false),
        ];
    }

    private function filterValidateFields( $fields = [] ) {
        $data_fields = $fields;

        foreach ( $fields as $k => $v ) {
            if ( !in_array($k, array_keys($this->validator)) ) {
                unset($data_fields[$k]);
            }
        }

        if(array_key_exists('archived', $data_fields)){
            $data_fields['archived'] = (int) filter_var($data_fields['archived'], FILTER_VALIDATE_BOOLEAN);
        }

        return $data_fields;

        //Works in PHP 5.6
        //return array_filter($fields, function ($key) {
        //        return in_array($key, array_keys($this->validator));
        //    },
        //    ARRAY_FILTER_USE_KEY
        //);
    }


    private function validateUpdate( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        // Only id is required
        v::intVal()->min(1)->assert($fields["id"]);

        // Assert only the fields that exist
        foreach( array_keys($data_fields) as $k ) {
            if ( $k === "id" )
                continue;

            $this->validator[$k]->assert($data_fields);
        }

        return $data_fields;
    }

    private function validateInsert( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        foreach ( $this->validator as $v ) {
            $v->assert($data_fields);
        }

        return $data_fields;
    }

    public function manageObject (Request $request, Response $response, array $args) {

        if ($request->isGet()) {
            $query = $request->getQueryParams();

            if (array_key_exists("id", $args)) {

                return $this->getObject(intval($args["id"]));

            } else {

                return $this->getObjects(
                    $this->getOffsetParam( $query ),
                    $this->getLimitParam( $query ),
                    $this->getFilterParam( $query ),
                    $this->getSortParam( $query )
                );

            }

        } else if ($request->isPost()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // NOTE: This is not technically correct to HTTP standard
            // PATCH should be used for an update
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);

            } else {

                return $this->createObject($fields);

            }

        } else if ($request->isPatch()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // Essentially this is the same as POST with an id
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);
            }

        } else if ($request->isDelete()) {
            if (array_key_exists("id", $args)) {

                return $this->deleteObject(intval($args["id"]));

            }
        }

        // Shouldn't be here
        throw new \Exception("Unknown request");
    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        $objects = array_map( array($this, "getUserRelationship"), parent::getObjects($offset, $limit, $filters) );
        return array_map( array($this, "convertData"), $objects );
    }

    public function getObject($id) {
        $object = $this->getUserRelationship( parent::getObject($id) );
        return $this->convertData($object);
    }

    private function convertData($o){
        if (array_key_exists("archived", $o)) {
            $o['archived'] = filter_var($o['archived'], FILTER_VALIDATE_BOOLEAN);
        }

        return $o;
    }

    private function getUserRelationship($o) {
        if (is_array($o) && array_key_exists("user_id", $o)) {
            unset($o["user_id"]);
        }

        return $o;
    }

    public function setObject($id, $fields=[]) {
        try {
            return parent::setObject($id,
                $this->validateUpdate(array_merge( $fields, [ "id" => intval($id) ]))
            );
        } catch (\Exception $e) {
            if ( $e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException ) {
                // Ignore if duplicate
                return true;
            } else {
                throw $e;
            }
        }
    }

    public function createObject($fields=[]) {
        if(array_key_exists('archived', $fields)){
            $fields['archived'] = (int) filter_var($fields['archived'], FILTER_VALIDATE_BOOLEAN);
        }

        // Mimic insert or update
        try {
            return parent::createObject( $this->validateInsert($fields) );
        } catch (\Exception $e) {
            if ( $e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException ) {
                $array = parent::getObjects(
                    0,
                    1,
                    ["and" => ["number" => ["eq" => $fields["number"]]]]
                );
                $contact = array_pop($array);

                // Set object instead of create
                parent::setObject($contact["id"], $fields);

                // Ignore if duplicate and return the matching object
                return intval($contact["id"]);

            } else {
                throw $e;
            }
        }
    }
}

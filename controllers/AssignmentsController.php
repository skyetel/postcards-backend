<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

use \App\Helpers\QueryParamHelper;

class AssignmentsController extends DataObjectController {
    const TABLE = "phonenumbers";

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Set the DAO table
        $this->table = self::TABLE;

        // TODO: Move this into a Data Model masked by a Data Model Authorization
        $this->validator = [
            "user_id" => v::key('user_id', v::oneOf( v::equals(''), v::nullType(), v::intVal()->min(1) ) )
        ];
    }

    private function filterValidateFields( $fields = [] ) {
        $data_fields = $fields;

        foreach ( $fields as $k => $v ) {
            if ( !in_array($k, array_keys($this->validator)) ) {
                unset($data_fields[$k]);
            }
        }

        return $data_fields;

        //Works in PHP 5.6
        //return array_filter($fields, function ($key) {
        //        return in_array($key, array_keys($this->validator));
        //    },
        //    ARRAY_FILTER_USE_KEY
        //);
    }


    private function validateUpdate( $fields = [] ) {
        // Make a copy of the arguments without unknown fields
        $data_fields = $this->filterValidateFields($fields);

        // Only id is required
        v::intVal()->min(1)->assert($fields["id"]);

        // Special case change user_id => "" to null if it exists
        if ( array_key_exists("user_id", $data_fields) && $data_fields["user_id"] === "" ) {
            $data_fields["user_id"] = null;
        }

        // Assert only the fields that exist
        foreach( array_keys($data_fields) as $k ) {
            if ( $k === "id" )
                continue;

            $this->validator[$k]->assert($data_fields);
        }

        return $data_fields;
    }

    private function validateInsert( $fields = [] ) {
        throw new \Exception("Not implemented");
        return null;
    }

    public function manageObject (Request $request, Response $response, array $args) {

        if ($request->isPost()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // NOTE: This is not technically correct to HTTP standard
            // PATCH should be used for an update
            if (array_key_exists("id", $fields)) {

                $this->setObject(intval($fields["id"]), $fields);
            }

        } else if ($request->isPatch()) {
            $fields = $this->getDataFields( $request->getParsedBody() );

            if (array_key_exists("id", $args)) {
                $fields["id"] = $args["id"];
            }

            // Essentially this is the same as POST with an id
            if (array_key_exists("id", $fields)) {

                return $this->setObject(intval($fields["id"]), $fields);
            }

        } else if ($request->isDelete()) {
            if (array_key_exists("id", $args)) {
                // Delete just means unassign
                $this->setObject(intval($args["id"]), [ "user_id" => null ] );
            }
        }
            
        // Always return results
        $query = $request->getQueryParams();

        $u = new UsersController($this->getApp(), $this->getAuthorizor());

        // fudge the offsets, limits, and filters
        if ( array_key_exists("phonenumbers", $query) && is_array($query["phonenumbers"]) ) {
            $phonenumbers_query = $query["phonenumbers"];
        } else {
            $phonenumbers_query = [];
        }


        if ( array_key_exists("users", $query) && is_array($query["users"]) ) {
            $users_query = $query["users"];
        } else {
            $users_query = [];
        }

        return [ 
            "limit" => [
                "phonenumbers" => QueryParamHelper::getLimitParam( $phonenumbers_query ),
                "users" => QueryParamHelper::getLimitParam( $users_query )
            ],
            "offset" => [
                "phonenumbers" => QueryParamHelper::getOffsetParam( $phonenumbers_query ),
                "users" => QueryParamHelper::getOffsetParam( $users_query )
            ],
            "phonenumbers" => $this->getObjects(
                $this->getOffsetParam( $phonenumbers_query ),
                $this->getLimitParam( $phonenumbers_query ),
                $this->getFilterParam( $phonenumbers_query )
            ),
            "users" => $u->getObjects(
                $this->getOffsetParam( $users_query ),
                $this->getLimitParam( $users_query ),
                $this->getFilterParam( $users_query )
            )
        ];
    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        return array_map( array($this, "getUserRelationship"), parent::getObjects($offset, $limit, $filters) );
    }

    public function getObject($id) {
        return $this->getUserRelationship( parent::getObject($id) );
    }

    private function getUserRelationship($o) {
        if (array_key_exists("user_id", $o)) {
            $u = new UsersController($this->getApp(), $this->getAuthorizor());
            $o["user"] = $u->getObject($o["user_id"]);

            unset($o["user_id"]);
        } else if ( is_array($o) ) {
            $o["user"] = null;
        }

        return $o;
    }



    public function setObject($id, $fields=[]) {
        return parent::setObject($id,
            $this->validateUpdate($fields)
        );
    }

    public function createObject($fields=[]) {
        throw new \Exception("Not implemented");
        return null;
    }

    public function deleteObject($id) {
        throw new \Exception("Not implemented");
        return null;
    }
}


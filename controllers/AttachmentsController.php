<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

use \App\Integrations\SkyetelSMSIntegration;

class AttachmentsController extends DataObjectController {
    const TABLE = "attachments";

    function __construct ($app, $authorizor = null) {
        parent::__construct($app, $authorizor);

        // Set the DAO table
        $this->table = self::TABLE;
    }

    public function manageObject (Request $request, Response $response, array $args) {
        throw new \Exception("Not implemented");
    }

    public function getObjects($offset = 0, $limit = parent::DEFAULT_LIMIT, $filters = []) {
        return array_map( array($this, "getMessageRelationship"), parent::getObjects($offset, $limit, $filters) );
    }

    public function getObject($id) {
        return $this->getMessageRelationship( parent::getObject($id) );
    }

    public function setObject($id, $fields=[]) {
        throw new \Exception("Not implemented");
    }

    public function createObject($fields=[]) {
        return parent::createObject( $fields );
    }

    public function deleteObject($id) {
        throw new \Exception("Not implemented");
    }

    private function getMessageRelationship($o) {
        return $o;
        if (array_key_exists("message_id", $o)) {
            //$m = new MessagesController($this->getApp(), $this->getAuthorizor());
            $o["message"] = $m->getObject($o["user_id"]);

            unset($o["message_id"]);
        } else if ( is_array($o) ) {
            $o["message"] = null;
        }

        return $o;
    }
}


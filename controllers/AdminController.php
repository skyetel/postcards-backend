<?php
namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Respect\Validation\Validator as v;

use \App\Helpers\QueryParamHelper;
use \App\Helpers\StatementFilter;

class AdminController {
    private $app;
    private $c;

    private $allowed_controllers = [ "users", "phonenumbers", "assignments", "dialogs", "sendtest", "smtp"];

    public function __construct( $app ) {
        $this->app = $app;
        $this->c = $app->getContainer();

        $this->app->get('[/]', function (Request $request, Response $response, array $args) {
            return $response->withRedirect($this->router->pathFor('admin-management',
                    [ "page" => "users" ]
                )
            );
        });

        $this->app->map(["GET", "POST"], '/management/{page}[/{id}]', array($this, 'manageObject'))
            ->setName('admin-management');

        $this->app->map(["GET"], '/management/dialogs/{id}/messages', array($this, 'getDialogMessages'))
            ->setName('admin-dialog-messages');

        $this->app->get('/logout', array($this, 'logout'))
            ->setName('admin-logout');

        $this->app->map(["GET", "POST"], '/login', array($this, 'login'))
            ->setName('admin-login');
    }

    public function logout(Request $request, Response $response, array $args) {
        session_unset();
        return $response->withRedirect($this->c->router->pathFor('admin-login'));
    }

    public function login (Request $request, Response $response, array $args) {
        if ($request->isGet()) {
            // Supply feedback message
            switch ($request->getQueryParam("message")) {
            case "login-failed":
                $message = "<b>Login failed</b>";
                break;
            default:
                $message = "";
            }

        } else if ($request->isPost()) {
            if (isset($_SESSION["__user"])) {
                // Already logged in
                return $response->withRedirect($this->c->router->pathFor('admin-management'),
                    [ "page" => "users" ]);
            }

            $data = $request->getParsedBody();

            $password = empty(getenv("ADMIN_BACKEND_PASSWORD"))
                ? null
                : getenv("ADMIN_BACKEND_PASSWORD");

            if (!is_null($password) && $data["username"] === "admin"
               && $data["password"] === $password ) {

                $_SESSION["__user"] = $data["username"];
                return $response->withRedirect(
                    $this->c->router->pathFor('admin-management', [ "page" => "users" ])
                );
            }

            return $response->withRedirect(
                $this->c->router->pathFor('admin-login') . "?message=login-failed"
            );
        }


        return $this->c->view->render($response, 'login.phtml', [ "message" => $message ]);
    }

    function getDialogMessages (Request $request, Response $response, array $args) {
        $query = $request->getQueryParams();

        $filter = QueryParamHelper::getFilterParam( $query );
        $dialog_filter = [ "and" => [ "dialog_id" => [ "eq" => intval($args["id"]) ] ] ];

        // Work out the filter with the dialog id
        if ( count($filter) > 0 ) {
            if ( StatementFilter::isAssociativeArray($filter) ) {
                $filter = array_merge_recursive(
                    $filter,
                    $dialog_filter 
                );
            } else {
                $filter[] = [ "and" => $dialog_filter ];
            }
        } else {
            $filter = $dialog_filter;
        }

        $dialog = (new DialogsController($this->app))
            ->getObject($args["id"]);
        
        $messages = new MessagesController($this->app);

        $data = [
            "messages" => $messages->getObjects(
                QueryParamHelper::getOffsetParam( $query ),
                QueryParamHelper::getLimitParam( $query ),
                $dialog_filter,
                QueryParamHelper::getSortParam( $query )
            ),
            "dialog" => $dialog
        ];

        return $this->c->view->render($response, 'management.phtml',
            [
                "router" => $this->c->router,
                "query_string" => $request->getUri()->getQuery(),
                "page" => "dialogs",
                "data" => $data,
                "limit" => QueryParamHelper::getLimitParam($query),
                "offset" => QueryParamHelper::getOffsetParam($query),
                "id" => $args["id"]
            ]
        );
    }

    /**
     * Generic ManageObject within the AdminController namespace
     */
    function manageObject (Request $request, Response $response, array $args) {

        // Check if the object page is allowed for this controller
        if ( ! in_array($args["page"], $this->allowed_controllers) ) {
            return $response
                ->withStatus(403);
        }

        $controller_name = __NAMESPACE__ . '\\' . ucfirst(strtolower($args["page"])) . "Controller";
        $controller = new $controller_name($this->app);

        $error_message = "";
        $message = "";
        $id = array_key_exists("id", $args) ? $args["id"] : null;
        $data = [];

        try {
            $data = $controller->manageObject($request, $response, $args);

            // Presume an update if response is boolean or an id from an insert if it is an intval
            if ( is_bool($data) || is_integer($data) ) {
                // Try to retrieve data from body
                $parsed_body = $request->getParsedBody();

                if ( array_key_exists("data", $parsed_body) ) {
                    $data = $parsed_body["data"];
                }

                if ( $data == true ) {
                    $error_message = "Record updated";
                } else {
                    $error_message = "Unable to update or create a record";
                }
            }

            // Informative message returned
            if ( array_key_exists("message", $data) ) {
                $message = $data["message"];
            }

            // Re-throw a returned exception after parsing data
            if (array_key_exists("exception", $data)) {
                throw $data["exception"];
            }

        } catch (\Exception $e) {
            if ($e instanceof \Respect\Validation\Exceptions\ValidationException) {

                if ($e instanceof \Respect\Validation\Exceptions\NestedValidationException) {
                    $error_message = "";
                    foreach ($e->getMessages() as $m) {
                        $error_message .= "$m<br/>";
                    }
                } else {
                    $error_message = $e;
                }
            } else if ( $e instanceof \PDOException ) {
                $error_message = "Database error";
            } else if ($e instanceof \Doctrine\DBAL\DBALException) {

                // Specific DBAL Errors
                if ( $e instanceof \Doctrine\DBAL\Exception\UniqueConstraintViolationException ) {
                    $error_message = "Record already exists";
                } else {
                    $error_message = "Database error: " . get_class($e) . " Message: " . $e->getMessage();
                }
            } else {
                $error_message = "Unknown error: " . get_class($e) . " Message: " . $e->getMessage();
            }
        }

        // If something happens try to retrieve data from the body
        if ( count($data) == 0  && is_array($request->getParsedBody()) ) {
            // Try to retrieve data from body
            $parsed_body = $request->getParsedBody();

            if ( array_key_exists("data", $parsed_body) ) {
                $data = $parsed_body["data"];
            }
        }

        // Requested size of page
        $query = $request->getQueryParams();
        $offset = QueryParamHelper::getOffsetParam( $query );
        $limit = QueryParamHelper::getLimitParam( $query );

        return $this->c->view->render($response, 'management.phtml',
            [
                "router" => $this->c->router,
                "query_string" => $request->getUri()->getQuery(),
                "page" => $args["page"],
                "data" => $data,
                "offset" => $offset,
                "limit" => $limit,
                "error_message" => $error_message,
                "message" => $message,
                "id" => $id
            ]
        );
    }
}

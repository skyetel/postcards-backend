# Postcards Backend

This is all the gears, switches, and other widgets that load and save data that the Postcards application needs to run perfectly.

## A little more technical

In order, for the User Interface to send, receive, and display SMS and MMS dialogs it needs to talk with this backend application. In turn, the backend application knows how to get just the right data and save it properly. Also, since an administrator needs to perform some very important steps to setup the Postcards Application, the backend provides a very basic interface for the admin to create new users, phonenumbers, and assign them together to be used later by the User Interface.

This backend application is written with PHP and supported by a few open source libraries that help make all the these things work together a little more smoothly.

<?php
require_once __DIR__.'/vendor/autoload.php';

use App\Controllers\AnnouncementsController;
use App\Integrations\SkyetelSMSIntegration;
use Doctrine\DBAL\DriverManager;

date_default_timezone_set('UTC');

$smsHandler = new SkyetelSMSIntegration(
    ( getenv("SKYETEL_SMS_API_URL") === false ? "https://sms.skyetel.com/v1/out" : getenv("SKYETEL_SMS_API_URL") ),
    getenv("SKYETEL_API_SID"),
    getenv("SKYETEL_API_SECRET")
);

$connectionParams = array(
    'dbname' => 'sms_mms_app',
    'user' => 'root',
    'password' => '',
    'host' => 'database',
    'driver' => 'pdo_mysql',
);

$connection = DriverManager::getConnection($connectionParams, $config);
$queryBuilderSelect = $connection->createQueryBuilder();
$queryBuilderUpdate = $connection->createQueryBuilder();

$process_id = uniqid('', true);

$queryBuilderUpdate->update('announcements')
    ->where("(scheduled IS NULL OR scheduled <= now())")
    ->andWhere("state = 'scheduled'")
    ->andWhere("process_id IS NULL")
    ->set("process_id", ":process_id")
    ->setParameter(":process_id", $process_id)
    ->set('state', ':state')
    ->setParameter(':state', AnnouncementsController::STATE_SENDING)
    ->setMaxResults(1)
    ->execute();

$announcement = $queryBuilderSelect
    ->select('a.id, a.message, a.phonenumbers, b.number')
    ->from('announcements', "a")
    ->join("a", "phonenumbers", "b", "b.user_id = a.user_id AND b.active IS NOT NULL")
    ->join("a", "users", "u", "u.id = a.user_id AND u.active IS NOT NULL")
    ->andWhere("process_id = :process_id")
    ->setParameter(":process_id", $process_id)
    ->setMaxResults(1)
    ->execute()
    ->fetch();

if(!empty($announcement)){
    try {
        $announcement_id = $announcement['id'];
        $announcement_message = $announcement['message'];
        $announcement_from = $announcement['number'];
        $announcement_numbers = explode(',', $announcement['phonenumbers']);

        $failed_numbers = [];

        foreach ($announcement_numbers as $number) {
            try {
                $send_message = [
                    "to" => $number,
                    "text" => $announcement_message
                ];

                echo 'Sending message  to : ' . $number . ' from ' . $announcement_from;

                $result = $smsHandler
                    ->send($announcement_from,
                        $send_message
                    );

            } catch (\Exception $e) {
                echo 'Failed sending message to : ' . $number;
                $failed_numbers[] = $number;
            }

            sleep(2);
        }

        if (count($failed_numbers) > 0) {
            if (count($failed_numbers) === count($announcement_numbers)) {
                $announcement_state = AnnouncementsController::STATE_FAILED;
            }
            else {
                $announcement_state = AnnouncementsController::STATE_PARTIAL_SUCCESS;
            }
        }
        else {
            $announcement_state = AnnouncementsController::STATE_SUCCESS;
        }

        $failed_numbers = count($failed_numbers) > 0 ? implode(',', $failed_numbers) : null;

        $queryBuilderUpdate->update('announcements')
            ->where("id = :id")
            ->setParameter(":id", $announcement_id)
            ->set('state', ':state')
            ->setParameter(':state', $announcement_state)
            ->set('failed_numbers', ':failed_numbers')
            ->setParameter(':failed_numbers', $failed_numbers)
            ->execute();

    } catch (\Exception $e) {
        echo 'Failed to send announcement ' . $announcement_id;

        $queryBuilderUpdate->update('announcements')
            ->where("id = :id")
            ->setParameter(":id", $announcement_id)
            ->set('state', ':state')
            ->setParameter(':state', AnnouncementsController::STATE_FAILED)
            ->execute();
    }
}
else {
    echo 'No announcements found';
}

ob_flush();
flush();
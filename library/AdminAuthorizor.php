<?php
namespace App\Auth;

class AdminAuthorizor extends DataObjectAuthorizor {
    public function authorizeSelect($qb) {
        return parent::authorizeSelect($qb);
    }

    public function authorizeUpdate($qb, $fields) {
        return parent::authorizeUpdate($qb, $fields);
    }

    public function authorizeInsert($qb, $fields) {
        return parent::authorizeInsert($qb, $fields);
    }

    public function authorizeDelete($qb) {
        return parent::authorizeDelete($qb);
    }
}

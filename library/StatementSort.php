<?php
namespace App\Data;

class StatementSort {
    // PHP 5.5 does not allow const arrays; In the future these can be set to const
    public static $OPERATORS = [ "and", "or" ];
    public static $EXPRESSIONS = [ "lt", "gt", "ne", "like" ];

    /**
     * filter[FIELD][or|and][lt|gt|ne|like] = <VALUE>   (comparison and/or)
     * filter[FIELD][or|and] = <VALUE>                  (equals and/or)
     * filter[FIELD][lt|gt|ne|like] = <VALUE>           (comparison and)
     * filter[FIELD] = <VALUE>                          (equals)
     */
    static function setSort($stmt, $sort = []) {
        $ret_stmt = $stmt;

        foreach($sort as $field) {
            // Filter the field name to remove +/- and make it lower case
            $field_name = strtolower(preg_replace('/[^a-zA-Z0-9_\.]*/', '', $field));

            if ( substr($field, 0, 1) === "-" ) {
                $direction = "DESC";
            } else {
                $direction = "ASC";
            }

            $ret_stmt = $ret_stmt
                ->addOrderBy($field_name, $direction);
        }

        return $ret_stmt;
    }
}

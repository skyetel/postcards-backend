<?php
namespace App\Helpers;

class QueryParamHelper {
    // Maximum returned records
    const MAX_LIMIT = 100;

    // Default number of returned records
    const DEFAULT_LIMIT = 10;

    static public function getDataFields($fields = []) {
        return array_key_exists("data", $fields) && is_array($fields["data"]) ? $fields["data"] : [];
    }

    static public function getFilterParam($query = []) {
        return array_key_exists("filter", $query) && is_array($query["filter"]) ? $query["filter"] : [];
    }

    static public function getOffsetParam($query = []) {
        return array_key_exists("offset", $query) && is_numeric($query["offset"]) ? intval($query["offset"]) : 0;
    }

    static public function getLimitParam($query = []) {
        return min( array_key_exists("limit", $query) && is_numeric($query["limit"]) ? intval($query["limit"]) : SELF::DEFAULT_LIMIT,
            self::MAX_LIMIT );
    }

    static public function getSortParam($query = []) {
        return array_key_exists("sort", $query) && is_string($query["sort"]) ? explode(",", $query["sort"]) : [];
    }

}

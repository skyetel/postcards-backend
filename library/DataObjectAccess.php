<?php
namespace App\Data;

interface DataObjectAccess {
    public function getObjects($offset, $limit, $filters);
    public function getObject($id);
    public function setObject($id, $fields);
    public function createObject($fields);
    public function deleteObject($id);
}

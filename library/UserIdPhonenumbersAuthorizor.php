<?php
namespace App\Auth;

class UserIdPhonenumbersAuthorizor extends DataObjectAuthorizor {
    private $user_id;

    function __construct($user_id) {
        parent::__construct();

        $this->user_id = $user_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function authorizeSelect($qb) {
        return parent::authorizeSelect($qb)
            ->select("a.*")
            ->andWhere("a.user_id = :authorized_user_id")
            ->setParameter(":authorized_user_id", intval($this->user_id));
    }

    public function authorizeUpdate($qb, $fields) {
        return parent::authorizeUpdate($qb, $fields)
            ->andWhere("a.user_id = :authorized_user_id")
            ->setParameter(":authorized_user_id", intval($this->user_id));
    }

    public function authorizeInsert($qb, $fields) {
        return parent::authorizeInsert($qb, 
            array_merge($fields, [ "user_id" => intval($this->user_id) ])
        );
    }

    public function authorizeDelete($qb) {
        return parent::authorizeDelete($qb)
            ->andWhere("a.user_id = :authorized_user_id")
            ->setParameter(":authorized_user_id", intval($this->user_id));
    }
}

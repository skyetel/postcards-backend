<?php
namespace App\Helpers;

use \App\Exceptions\AttachmentUnavailableException;
use \Slim\Http\UploadedFile;

class FileHelper {

    static public function getAttachmentPath($filename, $directory = "/var/www/attachments") {
        return $directory . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * Create a random path to a file in a directory where a file does not exist
     */
    static public function generateRandomFileNameHash($directory = "/var/www/attachments", $bytes = 24) {

        $i = 0;
        do {
            $file_hash = bin2hex(openssl_random_pseudo_bytes($bytes));

            // Retry if file exists
            if (!file_exists(self::getAttachmentPath($file_hash, $directory))) {
                return $file_hash;
            }
        } while ($i++ < 2);

        throw new AttachmentUnavailableException("Unable to create a random unique filename");
    
    }

    // From example: http://www.slimframework.com/docs/v3/cookbook/uploading-files.html
    static public function moveUploadedFile(UploadedFile $file, $directory = "/var/www/attachments") {
        $hash = self::generateRandomFileNameHash();

        $file->moveTo(self::getAttachmentPath($hash, $directory));

        return $hash;
    }

    static public function downloadURLtoFileHash($url, $directory = "/var/www/attachments") {
        $hash = self::generateRandomFileNameHash();

        $result = file_put_contents(self::getAttachmentPath($hash, $directory), fopen($url, 'r'));

        // Throw an Exception if nothing could be retrieved
        if ( $result === false || $result === 0 ) {
            // Clean up empty file if created
            if (file_exists(self::getAttachmentPath($hash, $directory))) {
                unlink(self::getAttachmentPath($hash, $directory));
            }

            throw new AttachmentUnavailableException("Unable to retrieve URL or save attachment");
        }

        return $hash;
    }

    static public function getImageDimensions($filename, $directory = "/var/www/attachments") {
        // Prepend directory path
        $filename = rtrim($directory, '/') . '/' . $filename;

        // Only allow images
        if ( !file_exists($filename) || strpos(mime_content_type($filename), "image/") !== 0) {
            return false;
        }
        $result = getimagesize($filename);

        // Filter
        $ret = [
            "width" => $result[0],
            "height" => $result[1]
        ];

        return $ret;
    }
}

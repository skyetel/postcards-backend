<?php
namespace App\Auth;

use \App\Controllers\DataObjectController;

abstract class DataObjectAuthorizor {
    private $readFields;
    private $writeFields;
    private $filter;

    function __construct($readFields = [], $writeFields = [], $filter = []) {
        $this->readFields = $readFields;
        $this->writeFields = $writeFields;
        $this->filter = $filter;
    }

    protected function getReadFields() {
        return $this->readFields;
    }

    protected function getWriteFields() {
        return $this->writeFields;
    }

    protected function getFilter() {
        return $this->filter;
    }

    public function authorizeSelect($qb) {
        return $qb
            ->select("*");
    }

    public function authorizeUpdate($qb, $fields) {
        $values = [];
        $params = [];

        foreach ($fields as $f => $v) {
            $values[$f] = ":$f";
            $params[":$f"] = $v;
        }

        foreach ($fields as $f => $v) {
            $qb->set($f, ":$f")
                ->setParameter(":$f", $v);
        }

        return $qb;
    }

    public function authorizeInsert($qb, $fields) {
        $values = [];
        $params = [];

        foreach ($fields as $f => $v) {
            $values[$f] = ":$f";
            $params[":$f"] = $v;
        }

        return $qb
            ->values( $values )
            ->setParameters( $params );
    }

    public function authorizeDelete($qb) {
        // Nothing to do without if no authorization
        return $qb;
    }

}

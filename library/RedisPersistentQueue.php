<?php
namespace App\Helpers;

class RedisPersistentQueue
{
    /**
     * @var Redis
     */
    private $redis;
    private $event_queue_name;

    public function __construct($event_queue_name)
    {
        $this->event_queue_name = $event_queue_name;
    }

    public function listen($worker_id, $topic, $callback)
    {
        $reconnect = false;
        $processing_queue = $this->event_queue_name . '-processing-' . $worker_id;
        do {
            try {
                $this->connect();
                $this->queuePastEvents($processing_queue);
                $this->waitForEvents($processing_queue, $callback);
            } catch (RedisException $e) {
                // we get that due to default_socket_timeout
                if (strtolower($e->getMessage()) === 'read error on connection') {
                    $reconnect = true;
                } else {
                    throw $e;
                }
            }

        } while ($reconnect === true);
    }

    /**
     * In case of crash of the worker, the processing queue might contain events not processed yet.
     *
     * This ensure events are re-queued on main event queue before going further
     */
    private function queuePastEvents($processing_queue)
    {
        do {
            $value = $this->redis->rpoplpush($processing_queue, $this->event_queue_name);
        } while ($value !== false);
    }

    private function waitForEvents($processing_queue, callable $callback)
    {
        while (true) {
            $value = $this->redis->brpoplpush($this->event_queue_name, $processing_queue, 0);
            $callback($value);
            $this->redis->lRem($processing_queue, $value, 1);
            sleep(5);
        }
    }

    private function connect()
    {
        $this->redis = new \Redis();
        $this->redis->connect('redis', 6379);
    }

    public function pushSinglePersistentMessage($topic, $content)
    {
        $this->connect();
        $this->redis->lPush(
            $this->event_queue_name,
            json_encode(
                [
                    'event_name' => $topic,
                    'payload' => $content,
                ]
            )
        );
    }

    public function pushMessage($topic, $content)
    {
        $this->connect();
        $this->redis->publish($this->event_queue_name, json_encode(
            [
                'event_name' => $topic,
                'payload' => $content,
            ]
        ));
    }
}
<?php
namespace App\Auth;

class PublicAttachmentsAuthorizor extends DataObjectAuthorizor {
    public function authorizeSelect($qb) {
        return parent::authorizeSelect($qb)
            ->select('*')
            ->andWhere("public_expiration > :now")
            ->setParameter(":now", (new \Datetime())->format("Y-m-d H:i:s"));
    }

    public function authorizeUpdate($qb, $fields) {
        throw new \Exception("Not implemented");
    }

    public function authorizeInsert($qb, $fields) {
        throw new \Exception("Not implemented");
    }

    public function authorizeDelete($qb) {
        throw new \Exception("Not implemented");
    }
}

<?php
namespace App\Authorization;

interface DataObjectAuthorizor {
    // TODO: Authorize WHERE clause ?
    function authorizeRead($stmt, $fields);
    function authorizeWrite($stmt, $fields);
}

<?php
namespace App\Helpers;

class StatementFilter {
    // PHP 5.5 does not allow const arrays; In the future these can be set to const
    public static $OPERATORS = [ "and", "or" ];
    public static $EXPRESSIONS = [ "eq", "lt", "gt", "ne", "like" ];

    /**
     * filter[#][and|or][...]                            Parenthese subqueries
     * ...[and|or][FIELD][eq|lt|gt|ne|like] = <VALUE>    (comparison and/or)
     *
     * Example #1:
     *
     * Query: WHERE (id > 10 and id < 15) AND (viewed = 1 AND id > 100)
     *
     * Query parameters
     * filter[0][and][id][gt]=10
     * filter[0][and][id][lt]=15
     * filter[1][and][viewed][eq]=1
     * filter[1][and][id][gt]=100
     *
     * Example #2:
     *
     * Query: WHERE id > 100 OR name LIKE '%bob%'
     *
     * Query parameter
     * filter[and][id][gt]=100
     * filter[or][name][like]=bob
     *
     */
    static function setFilter($stmt, array $filter = [] ) {
        // Skip empty filter
        if ( count($filter) === 0 ) {
            return $stmt;
        }

        // Process the filter input into an array structure
        if (self::isAssociativeArray($filter) ) {
            $process_filter = [ $filter ];
        } else {
            $process_filter = $filter;
        }

        $query_index = 1;
        $ret_stmt = $stmt;
        foreach ( $process_filter as $sub_query ) {
            $query_filter = self::getQueryParts($sub_query, $query_index++);

            // Always use "andWhere" or this will negate other security filters
            $ret_stmt = $ret_stmt
                ->andWhere($query_filter["query"]);

            // Preserve other parameters by merging
            $ret_stmt = $ret_stmt
                ->setParameters(array_merge($query_filter["parameters"], $ret_stmt->getParameters()));
        }

        return $stmt;
    }

    /**
     * filter[and|or][FIELD][eq|lt|gt|ne|like] = <VALUE>    (comparison or)
     * Return:
     *  [ "query": "x1 > :xq1i1 AND xx = :xxq1i1" , "parameters": [ ":xq1i1": "1", ":xxq1i1": "text" ] ]
     */
    static function getQueryParts(array $filter, $query_index = 1) {
        $ret_array = [];

        // TODO: Process this logic into query strings 

        $query_string = "";
        $parameter_array = [];
        $vi = 1;
        // First layer operator [and|or]
        foreach($filter as $op => $fields) {

            if (!in_array($op, self::$OPERATORS)) {
                // Skip invalid fields
                continue;
            }

            // Second layer expression [FIELD]
            if (is_array($fields)) {
                foreach ($fields as $field => $exprs) {

                    // Third layer expression [eq|lt|gt|ne|like]
                    if (is_array($exprs)) {
                        foreach ($exprs as $expr => $expr_value) {
                            // Final layer (value)
                            if ( in_array($expr, self::$EXPRESSIONS) ) {

                                $suffix = "q" . strval($query_index) . "v" . strval($vi++);
                                $placeholder = preg_replace("/[^A-Za-z0-9 ]/", '', $field) . $suffix;
                                $parameter_array[ ":" . $placeholder ] = $expr_value;

                                if ( $query_string === "" ) {
                                    $query_string = self::getExpressionString($field, $placeholder, $expr);
                                } else {
                                    $query_string .= " "
                                        . strtoupper($op) 
                                        . " "
                                        . self::getExpressionString($field, $placeholder, $expr);
                                }

                            } else {
                                // Invalid? Skip.
                                continue;
                            }
                        }
                    }
                }
            }
        }

        $ret_array = [ "query" => $query_string, "parameters" => $parameter_array ];  

        return $ret_array;
    }

    static function isAssociativeArray(array $arr) {
        if (array() === $arr) return false;

        return array_filter( array_keys($arr), 'is_int' ) !== array_keys($arr);
    }


    static public function mergeFilters($filter = [], $filter2 = []) {
        if ( count($filter) > 0 ) {
            if ( self::isAssociativeArray($filter) ) {
                $filter = array_merge_recursive(
                    $filter,
                    $filter2
                );
            } else {
                $filter[] = [ "and" => $filter2 ];
            }
        }
        else {
            $filter = $filter2;
        }

        return $filter;
    }

    static function getExpressionString($field, $value, $expression) {
        switch ($expression) {
        case "lt":
            return "$field < :$value";
            break;
        case "gt":
            return "$field > :$value";
            break;
        case "ne":
            return "$field != :$value";
            break;
        case "like":
            return "$field LIKE :$value";
            break;
        case "eq":
        default:
            return "$field = :$value";
            break;
        }
    }

    static function setStatementWhere($stmt, $field, $value, $value_placeholder, $expression = null, $operator = null) {
        $expression_string = self::getExpressionString($field, $value_placeholder, $expression);

        switch ($operator) {
        case "and":
            return $stmt->andWhere($expression_string)
                ->setParameter(":$value_placeholder", $value);
            break;

        case "or":
            return $stmt->orWhere($expression_string)
                ->setParameter(":$value_placeholder", $value);
            break;

        default:
            return $stmt->andWhere($expression_string)
                ->setParameter(":$value_placeholder", $value);
            break;
        }
    }
}

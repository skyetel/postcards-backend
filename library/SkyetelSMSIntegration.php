<?php
namespace App\Integrations;

use \Respect\Validation\Validator as v;

use \Httpful\Request;

class SkyetelSMSIntegration {
    private $sms_url;
    private $api_sid;
    private $api_secret;

    private $validation;

    function __construct($sms_url, $api_sid, $api_secret) {
        $this->sms_url = $sms_url;
        $this->api_sid = $api_sid;
        $this->api_secret = $api_secret;

        // Outbound SMS/MMS validation
        $this->validation = v::keySet(
                v::key('to', v::stringType()->digit()->length(5,15))
                    ->setTemplate("A valid destination number is required"),
                v::key('text', v::stringType()->length(null, 1024))
                    ->setTemplate("Text message with up to 1024 characters required"),
                v::key('media', v::arrayVal()->each( v::url() ), false)
                    ->setTemplate("List of one or more public URLs for attachments expected")
            );

    }

    public function send($from, $message) {
        // Validate the from phonenumber
        v::stringType()
            ->digit()
            ->startsWith("1")
            ->length(11, 11)
            ->setTemplate("From phone number must be a valid 11-digit number")
            ->assert($from);

        $request = Request::post($this->sms_url . "?" . "from=" . $from)
            ->timeout(3)
            ->sendsJson()
            ->expectsJson()
            ->basicAuth($this->api_sid, $this->api_secret)
            ->body(json_encode($message));

        $result = $request
            ->send();

        // Handle results
        if ($result->code === 200) {
            return $result->hasBody() ?
                $result->body
                : null;
        } else if ($result->code === 401) {
            throw new \Exception("Not authorized to send messages");
        } else {
            throw new \Exception("Received result code: " . $result->code);
        }
    }

}

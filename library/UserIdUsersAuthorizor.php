<?php
namespace App\Auth;

class UserIdUsersAuthorizor extends DataObjectAuthorizor {
    private $user_id;

    function __construct($user_id) {
        parent::__construct();

        $this->user_id = $user_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function authorizeUpdate($qb, $fields) {
        return parent::authorizeUpdate($qb, $fields)
            ->andWhere("a.id = :authorized_user_id")
            ->setParameter(":authorized_user_id", (int)$this->user_id);
    }

}

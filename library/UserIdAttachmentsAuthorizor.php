<?php
namespace App\Auth;

class UserIdAttachmentsAuthorizor extends DataObjectAuthorizor {
    private $user_id;

    function __construct($user_id) {
        parent::__construct();

        $this->user_id = $user_id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function authorizeSelect($qb) {
        return parent::authorizeSelect($qb)
            ->select("a.*")
            ->join("a", "messages", "b", "b.id = a.message_id")
            ->join("b", "dialogs", "c", "c.id = b.dialog_id")
            ->andWhere("c.user_id = :authorized_user_id")
            ->setParameter(":authorized_user_id", intval($this->user_id));
    }

    public function authorizeUpdate($qb, $fields) {
        return parent::authorizeUpdate($qb, $fields)
            ->join("a", "messages", "b", "b.id = a.message_id")
            ->join("b", "dialogs", "c", "c.id = b.dialog_id")
            ->andWhere("c.user_id = :authorized_user_id")
            ->setParameter(":authorized_user_id", intval($this->user_id));
    }

    public function authorizeInsert($qb, $fields) {
        return parent::authorizeInsert($qb, $fields);
    }

    public function authorizeDelete($qb) {
        return parent::authorizeDelete($qb)
            ->join("a", "messages", "b", "b.id = a.message_id")
            ->join("b", "dialogs", "c", "c.id = b.dialog_id")
            ->andWhere("c.user_id = :authorized_user_id")
            ->setParameter(":authorized_user_id", intval($this->user_id));
    }
}
